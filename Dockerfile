FROM docker.io/opendevorg/python-base:3.11-bookworm

COPY authdaemon.py /

ENTRYPOINT ["/authdaemon.py"]
CMD ["/authdaemon/token"]
